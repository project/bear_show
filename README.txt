About
-----------
Bear Slideshow is a features package that provides a responsive Flexslider Views Slideshow. It creates a Slideshow content type with slide image and slide text fields, a Block view display that can be displayed contextually by Node ID, and a Content Pane view display.

Instructions
-----------
View INSTALL.txt for specific instructions.
